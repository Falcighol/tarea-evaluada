<%-- 
    Document   : index
    Created on : 23/09/2018, 03:25:57 PM
    Author     : Juan Pablo Elias Hernández
--%>

<%@page import="java.time.LocalDateTime"%>
<%@page import="java.time.format.DateTimeFormatter"%>
<%@page import="com.models.Chart"%>
<%@page import="com.models.DaoCliente"%>
<%@page import="com.models.Cliente"%>
<%@page import="java.util.List"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true" %>

<% 
    HttpSession ss = request.getSession();
    Integer idTu = -1;
    String user = "";
    if (ss.getAttribute("tu") != null || ss.getAttribute("user") != null)
    {
        idTu = (Integer) ss.getAttribute("tu");
        user = (String) ss.getAttribute("user");
    }
    else
    {
        response.sendRedirect("login.jsp");
    }
    Cliente cli = new Cliente();
    DaoCliente daoC = new DaoCliente();
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" type="image/png" href="resources/img/Icon-shop.png" sizes="64x64">
        <link rel="stylesheet" href="resources\css\bootstrap.min.css">
        <title>Inicio</title>
    </head>
    <body>
        <style>
            .scroll-to-top {
                position: fixed;
                right: 20px;
                bottom: 20px;
                display: none;
                text-align: center;
                /*color: white;
                background: rgba(52, 58, 64, 0.7);
                line-height: 25px;*/
            }

            .scroll-to-top i {
                font-weight: 800;
            }
            
            @media only screen and (max-width: 600px) {
                .logo-img {
                    width: 70%;
                }
            }
        </style>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="index.jsp">
                <i class="fas fa-store"></i>
                Tienda
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">
                            <i class="fas fa-home    "></i>
                            Inicio <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="producto.jsp">
                            <i class="fas fa-shopping-basket"></i>
                            Productos
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="vendedor.jsp">
                            <i class="fas fa-id-card-alt"></i>
                            Vendedores
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="cliente.jsp">
                            <i class="fas fa-users"></i>
                            Clientes
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="venta.jsp">
                            <i class="fas fa-receipt"></i>
                            Venta
                        </a>
                    </li>
                    <%
                        if (idTu == 1)
                        {
                    %>
                    <li class="nav-item">
                        <a class="nav-link" href="usuario.jsp">
                            <i class="fas fa-user"></i>
                            Usuarios
                        </a>
                    </li>
                    <%
                        }
                    %>
                </ul>
                <form action="controllerUsuario" id="closeFrm" method="post" class="form-inline my-2 my-lg-0">
                    <input type="text" name="close" value="close" style="display: none;">
                    <button class="btn btn-outline-light btn-sm my-2 my-sm-0" 
                            name="close" value="close" type="button" id="closeSession">
                        <i class="fas fa-sign-out-alt"></i>
                        Cerrar sesion
                    </button>
                </form>
            </div>
        </nav>
        <div class="container">
            <div class="jumbotron" align="center" style="background-color: rgb(255, 255, 255);">
                <img class="logo-img" src="resources\img\ShopStore.png" alt="Logo" width="30%">
                <br>
                <br>
                <h1 class="display-4">Bienvenido al sistema, <%= user %>!</h1>
                <p class="lead">
                    Gestione las ventas, productos, clientes y vendedores de su tienda.
                    Todo a un solo click, optimizado para una mejor experiencia de venta y compra online.
                </p>
                <hr class="my-4">
                <h2 class="text-dark">Graficas del sistema</h2>
                <br>
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-6" style="padding-bottom: 1rem;">
                        <div class="card">
                            <div class="card-header">
                                <h5><i class="fa fa-chart-bar"></i> Gráfica de 5 productos con más stock actual</h5>
                            </div>
                            <div class="card-body">
                                <canvas id="pieChart"></canvas>
                            </div>
                            <div class="card-footer small text-muted">
                                Última actualización: 
                                <%
                                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
                                    String formatDateTime = LocalDateTime.now().format(formatter);
                                    out.print(formatDateTime);
                                %>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-6" style="padding-bottom: 1rem;">
                        <div class="card">
                            <div class="card-header">
                                <h5><i class="fa fa-chart-pie"></i> Gráfica de 5 vendedores con más ventas</h5>
                            </div>
                            <div class="card-body">
                                <canvas id="myChart"></canvas>
                            </div>
                            <div class="card-footer small text-muted">
                                Última actualización: 
                                <%
                                    DateTimeFormatter fmt = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
                                    String fdt = LocalDateTime.now().format(formatter);
                                    out.print(formatDateTime);
                                %>
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="my-4">
                <h2 class="text-dark">Reportes del sistema</h2>
                <br>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-3" style="padding-bottom: 1rem;">
                        <button class="btn btn-dark col-sm-12 report" role="button" data-url="reporteClientes.jsp">
                            <i class="fas fa-file-alt"></i> 
                            Rep. General de <br>clientes registrados
                        </button>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-3" style="padding-bottom: 1rem;">
                        <button class="btn btn-dark col-sm-12 report" role="button" data-url="reporteProductos.jsp">
                            <i class="fas fa-file-alt"></i> 
                            Rep. General de <br>productos registrados
                        </button>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-3" style="padding-bottom: 1rem;">
                        <button class="btn btn-dark col-sm-12 report" role="button" data-url="reporteProductosMasVendidos.jsp">
                            <i class="fas fa-file-alt"></i> 
                            Rep. Productos <br>más vendidos
                        </button>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-3" style="padding-bottom: 1rem;">
                        <button class="btn btn-dark col-sm-12 report" data-url="reporteVendedorMasVentas.jsp">
                            <i class="fas fa-file-alt"></i> 
                            Rep. Vendedores <br>con más ventas
                        </button>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-6" style="padding-bottom: 1rem;">
                        <div class="card">
                            <div class="card-header">
                                <h5>Reporte de facturas del cliente</h5>
                            </div>
                            <div class="card-body">
                                <form id="frmClient" class="needs-validation col-12" novalidate>
                                    <div class="form-group">
                                        <label for="cId">Seleccionar cliente:</label>
                                        <select id="cId" class="form-control" name="cId" required>
                                            <option value="">-- Seleccionar --</option>
                                        <%
                                            List <Cliente> lsC = daoC.mostrarCliente();
                                            for (Cliente c : lsC)
                                            {
                                        %>
                                            <option value="<%= c.getId() %>"><%= c.getNombre() %></option>
                                        <%
                                            } 
                                        %>
                                        </select>
                                        <div class="invalid-feedback">
                                            Por favor, seleccione un cliente.
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-sm-12 col-md-6">
                                            <button type="button" id="btnSendClient" class="btn btn-dark col-sm-12">
                                                <i class="fas fa-file-alt"></i> 
                                                Ver reporte
                                            </button>
                                        </div>
                                        <div class="form-group col-sm-12 col-md-6">
                                            <button type="reset" id="reset" class="btn btn-light col-sm-12">
                                                <i class="fas fa-undo"></i> 
                                                Cancelar
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>      
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-6" style="padding-bottom: 1rem;">
                        <div class="card">
                            <div class="card-header">
                                <h5>Reporte de facturas en un rango de fechas</h5>
                            </div>
                            <div class="card-body">
                                <form id="frmDates" class="needs-validation col-12" novalidate>
                                    <div class="form-row">
                                        <div class="form-group col-sm-12 col-md-6">
                                            <label for="cId">Fecha inicial:</label>
                                            <input type="date" id="f1" class="form-control" required>
                                            <div class="invalid-feedback">
                                                Por favor, seleccione una fecha inicial.
                                            </div>
                                        </div>
                                        <div class="form-group col-sm-12 col-md-6">
                                            <label for="cId">Fecha final</label>
                                            <input type="date" id="f2" class="form-control" required>
                                            <div class="invalid-feedback">
                                                Por favor, seleccione una fecha final.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-sm-12 col-md-6">
                                            <button type="button" id="btnSendDates" class="btn btn-dark col-sm-12">
                                                <i class="fas fa-file-alt"></i> 
                                                Ver reporte
                                            </button>
                                        </div>
                                        <div class="form-group col-sm-12 col-md-6">
                                            <button type="reset" id="reset" class="btn btn-light col-sm-12">
                                                <i class="fas fa-undo"></i> 
                                                Cancelar
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="my-4">
                <h2 class="text-dark">Gestion de modulos</h2>
                <br>
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-3" style="padding-bottom: 1rem;">
                        <a class="btn btn-dark col-sm-12" href="venta.jsp" 
                           role="button">
                            <i class="fas fa-receipt"></i>&nbsp; 
                            Ir a ventas
                        </a>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-3" style="padding-bottom: 1rem;">
                        <a class="btn btn-dark col-sm-12" href="cliente.jsp" 
                           role="button">
                            <i class="fas fa-users"></i>&nbsp; 
                            Ir a Clientes
                        </a>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-3" style="padding-bottom: 1rem;">
                        <a class="btn btn-dark col-sm-12" href="vendedor.jsp" 
                           role="button">
                            <i class="fas fa-id-card-alt"></i>&nbsp; 
                            Ir a Vendedores 
                        </a>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-3" style="padding-bottom: 1rem;">
                        <a class="btn btn-dark col-sm-12" href="producto.jsp" 
                           role="button">
                            <i class="fas fa-shopping-basket"></i>&nbsp; 
                            Ir a Productos
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Scroll to Top Button-->
        <button class="scroll-to-top rounded btn btn-outline-dark btn-lg">
            <i class="fa fa-angle-up"></i>
        </button>
        <script src="resources/js/jquery-3.3.1.js" type="text/javascript"></script>
        <script src="resources/js/popper.min.js" type="text/javascript"></script>
        <script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="resources/js/fontawesome-all.js" type="text/javascript"></script>
        <script src="resources/js/sweetalert2.all.min.js" type="text/javascript"></script>
        <script src="resources/js/index.js" type="text/javascript"></script>
        <script src="resources/js/globalConfig.js" type="text/javascript"></script>
        <script src="resources/js/charts.js" type="text/javascript"></script>
        <%
            Chart c = new Chart();
        %>
        <script>
            (function () 
            {
                var ctx = $("#pieChart");
                var myChart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: <%= c.getProductName() %>,
                        datasets: [{
                            label: 'Stock actual',
                            data: <%= c.getProductStock() %>,
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.8)',
                                'rgba(54, 162, 235, 0.8)',
                                'rgba(255, 206, 86, 0.8)',
                                'rgba(75, 192, 192, 0.8)',
                                'rgba(153, 102, 255, 0.8)'
                            ],
                            borderColor: [
                                'rgba(255, 99, 132, 1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 102, 255, 1)'
                            ],
                            borderWidth: 1
                        }]
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                  min: 0,
                                  maxTicksLimit: 5
                                },
                                gridLines: {
                                  display: true
                                },
                                beginAtZero:true
                            }]
                        },
                        legend: {
                          display: false
                        }
                    }
                });
            })(jQuery);
            var ctx = $("#myChart");
            var myChart = new Chart(ctx, {
                type: 'pie',
                data: {
                    labels: <%= c.getSellerName() %>,
                    datasets: [{
                        label: 'Total ventas',
                        data: <%= c.getSellerCant() %>,
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.8)',
                            'rgba(54, 162, 235, 0.8)',
                            'rgba(255, 206, 86, 0.8)',
                            'rgba(75, 192, 192, 0.8)',
                            'rgba(153, 102, 255, 0.8)'
                        ],
                        borderColor: [
                            'rgba(255, 99, 132, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)'
                        ],
                        borderWidth: 1
                    }]
                }
            });
        </script>
    </body>
</html>
