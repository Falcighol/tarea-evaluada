<%-- 
    Document   : reporteProductos
    Created on : 10-03-2018, 07:00:45 PM
    Author     : Perez
--%>

<%@page import="net.sf.jasperreports.engine.*" %>
<%@page import="java.util.Map" %>
<%@page import="java.util.HashMap" %>
<%@page import="java.io.File" %>
<%@page import="com.connection.Conexion" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="icon" type="image/png" href="resources/img/Icon-shop.png" sizes="64x64">
        <title>Reporte productos</title>
    </head>
    <body>
        <h1 align="center">Reporte de productos</h1>
        <div align="center">
        <%
            Conexion c = new Conexion();
            c.conectar();
            File reporte = new File(application.getRealPath("reports/productos.jasper"));
            Map parametros = new HashMap();
            byte[] bytes = JasperRunManager.runReportToPdf(reporte.getPath(), parametros, c.getConn());
            response.setContentType("application/pdf");
            response.setContentLength(bytes.length);
            ServletOutputStream output  = response.getOutputStream();
            output.write(bytes,0,bytes.length);
            output.flush();
            out.close();
        %>
        </div>
    </body>
</html>
