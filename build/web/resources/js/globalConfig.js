(function () {
    
    $('#closeSession').click(function()
    {
        console.log('Click on close');
        closeSession();
    });
    
})(jQuery);

function closeSession()
{
    swal({
        title: 'Seguro que desea cerrar la sesion?',
        text: "Todos los cambios no guardados se perderan.",
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: '#138496',
        cancelButtonColor: '#5A6268',
        confirmButtonText: 'Cerrar sesión',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.value)
        {
            $('#closeFrm').submit();
        }
    });
}
