(function()
{
    $('.select').click(function (e)
    {
        //window.scrollTo(0, 0);
        $('html, body').animate({scrollTop:0}, 'slow');
    });
    
    $('.btnAction').click(function (e)
    {
        var form = $('#frm')[0];
        sendData(form, $(this), e);
    });
    
    validateForm();
    
    // Inicializando tabla
    $("#cliTable").DataTable({
        "language": {
            "sProcessing": "Procesando...",
            "lengthMenu": "Mostrar _MENU_ registros por pagina",
            "zeroRecords": "No se encontraron resultados",
            "info": "Mostrando pagina _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrando de un total de _MAX_ registros)",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "sSearch": "Buscar:",
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });
})(jQuery);

// Funcion para cargar datos
function load(id, nombre, apellido, direccion)
{
    $("#cliId").val(id);
    $("#nom").val(nombre);
    $("#ape").val(apellido);
    $("#dir").val(direccion);
}

function validateForm()
{
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = $('.needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
          if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
          }
          form.classList.add('was-validated');
        }, false);
    });
}

// Funcion para enviar datos
function sendData(form, btn, e)
{
    if (form.checkValidity() === false)
    {
        e.preventDefault();
        e.stopPropagation();
    }
    else
    {
        swal({
            title: 'Seguro que desea ' + btn.val() + ' el registro?',
            text: "Si selecciona 'Aceptar', la transaccion será realizada.",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#138496',
            cancelButtonColor: '#5A6268',
            confirmButtonText: 'Aceptar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.value)
            {
                $('#action').val($(btn).val());
                $('#frm').submit();
            }
        });
    }
    form.classList.add('was-validated');
}
