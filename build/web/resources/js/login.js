(function ()
{
    $('#btnLogin').click(function (e)
    {
        sendData($('#frm')[0], e);
    });
})(jQuery);

// Funcion para enviar datos
function sendData(form, e)
{
    if (form.checkValidity() === false)
    {
        e.preventDefault();
        e.stopPropagation();
    }
    else
    {
        $('#frm').submit();
    }
    form.classList.add('was-validated');
}

function showError(error)
{
    $('.alert').append(error);
    $('.alert').slideDown('slow');
    setTimeout(function(){
            $('.alert').slideUp('slow');
    }, 4000);
}