<%-- 
    Document   : detalle
    Created on : 3/10/2018, 02:18:18 AM
    Author     : Juan Pablo Elias Hernández
--%>

<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.models.*"%>
<%@page import="java.time.LocalDateTime"%>
<%@page session="true" %>

<% 
    HttpSession ss = request.getSession();
    Integer idTu = -1;
    String user = "";
    if (ss.getAttribute("tu") != null || ss.getAttribute("user") != null)
    {
        idTu = (Integer) ss.getAttribute("tu");
        user = (String) ss.getAttribute("user");
    }
    else
    {
        response.sendRedirect("login.jsp");
    }
    DaoDetalleFactura daoDf = new DaoDetalleFactura();
    DaoFactura daoF = new DaoFactura();
    Factura f = new Factura();
    Integer state = -1;
    DetalleFactura df = new DetalleFactura();
    if (request.getSession().getAttribute("cF") != null 
            || request.getParameter("cF") != null
            || request.getParameter("n") != null)
    {
    String num = "";
    if (request.getSession().getAttribute("cF") != null)
    {
        num = (String) request.getSession().getAttribute("cF");
        state = 0;
    }
    else if (request.getParameter("cF") != null)
    {
        num = request.getParameter("cF");
        state = 1;
    }
    else if (request.getParameter("n") != null)
    {
        num = request.getParameter("n");
        state = 1;
    }
    f = daoF.findOne(num);
    List <Producto> lsP;
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" type="image/png" href="resources/img/Icon-shop.png" sizes="64x64">
        <link rel="stylesheet" href="resources\css\bootstrap.min.css">
        <link href="resources/css/sweetalert2.min.css" rel="stylesheet" type="text/css"/>
        <link href="resources/css/datatables.css" rel="stylesheet" type="text/css"/>
        <link href="resources/css/dataTables.bootstrap4.css"/>
        <link href="resources/css/buttons.bootstrap4.css" rel="stylesheet" type="text/css"/>
        <title>Venta</title>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="#">
                <i class="fas fa-store"></i>
                Tienda
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="index.jsp">
                            <i class="fas fa-home    "></i>
                            Inicio <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="producto.jsp">
                            <i class="fas fa-shopping-basket"></i>
                            Productos
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="vendedor.jsp">
                            <i class="fas fa-id-card-alt"></i>
                            Vendedores
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="cliente.jsp">
                            <i class="fas fa-users"></i>
                            Clientes
                        </a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="venta.jsp">
                            <i class="fas fa-receipt"></i>
                            Venta 
                            <i class="fas fa-angle-right"></i> 
                            <i class="fas fa-align-left"></i>
                            Detalle
                        </a>
                    </li>
                    <%
                        if (idTu == 1)
                        {
                    %>
                    <li class="nav-item">
                        <a class="nav-link" href="usuario.jsp">
                            <i class="fas fa-user"></i>
                            Usuarios
                        </a>
                    </li>
                    <%
                        }
                    %>
                </ul>
                <form action="controllerUsuario" id="closeFrm" method="post" class="form-inline my-2 my-lg-0">
                    <input type="text" name="close" value="close" style="display: none;">
                    <button class="btn btn-outline-light btn-sm my-2 my-sm-0" 
                            name="close" value="close" type="button" id="closeSession">
                        <i class="fas fa-sign-out-alt"></i>
                        Cerrar sesion
                    </button>
                </form>
            </div>
        </nav>
        <div class="container">
            <br>
            <br>
            <h1 align="center" class="text-dark">
                <i class="fas fa-receipt"></i>
                Detalle de factura / venta
            </h1>
            <br>
            <%
                if (idTu == 1 || idTu == 2)
                {
            %>
            <form action="controllerDetalleFactura" data-state='<%= state %>' id="frm" method="post" class="needs-validation" novalidate>
                <div class="form-row">
                    <input type="number" id="fId" value="<%= f.getId() %>" name="fId" style="display: none;">
                    <input type="text" name="action" id="action" style="display: none;"/>
                    <div class="form-group col-sm-12 col-md-3">
                        <label for="num">Codigo Factura:</label>
                        <input type="text" value="<%= f.getNumFactura() %>" id="num" class="form-control" 
                               name="num" required readonly>
                        <div class="invalid-feedback">
                            Por favor, seleccione una factura.
                        </div>
                    </div>
                    <div class="form-group col-sm-12 col-md-3">
                        <label for="cli">Cliente:</label>
                        <input type="text" id="cli" class="form-control" name="cli" 
                               value="<%= f.getCliente().getNombre()%>" required readonly>
                        <div class="invalid-feedback">
                            Por favor, seleccione una factura.
                        </div>
                    </div>
                    <div class="form-group col-sm-12 col-md-3">
                        <label for="ven">Vendedor:</label>
                        <input type="text" id="ven" class="form-control" name="ven" 
                               value="<%= f.getVendedor().getNombre() %>" required readonly>
                        <div class="invalid-feedback">
                            Por favor, seleccione una factura.
                        </div>
                    </div>
                    <div class="form-group col-sm-12 col-md-3">
                        <label for="tot">Total:</label>
                        <input type="text" id="tot" class="form-control" name="tot" 
                               value="<%= f.getTotal() %>" required readonly>
                        <div class="invalid-feedback">
                            Por favor, seleccione una factura.
                        </div>
                    </div>
                    
                </div>
                <div class="form-row">
                    <input type="number" id="dfId" name="dfId" value="0" style="display: none;">
                    <div class="form-group col-sm-12 col-md-3">
                        <label for="pId">Producto:</label>
                        <select id="pId" class="form-control changeControls" name="pId" required>
                            <option value="">-- Seleccionar --</option>
                        <%
                            lsP = daoDf.findProductos();
                            int i = 0;
                            for (Producto p : lsP)
                            {
                        %>
                                <option value="<%= i %>"><%= p.getNombre() %></option>
                        <%
                                i++;
                            } 
                        %>
                        </select>
                        <input type="text" id="pro" name="pro" style="display: none;">
                        <div class="invalid-feedback">
                            Por favor, seleccione un producto.
                        </div>
                    </div>
                    <div class="form-group col-sm-12 col-md-3">
                        <label for="cant">Cantidad:</label>
                        <input type="number" id="cant" value="0" min="1"
                            class="form-control changeControls" name="cant" required>
                        <div class="invalid-feedback">
                            Por favor, digite una cantidad mayor a 0.
                        </div>
                    </div>
                    <div class="form-group col-sm-12 col-md-3">
                        <label for="pre">Precio Unitario:</label>
                        <input type="number" id="pre" step="any" value="0.0" 
                               class="form-control" name="pre" required readonly>
                        <div class="invalid-feedback">
                            Por favor, seleccione un producto.
                        </div>
                    </div>
                    <div class="form-group col-sm-12 col-md-3">
                        <label for="sub">Subtotal:</label>
                        <input id="sub" class="form-control" step="any" value="0.0" 
                               name="sub" required readonly>
                        <div class="invalid-feedback">
                            Por favor, seleccione un producto.
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <button type="submit" class="btn btn-outline-success" id="btnAdd" name='btnAdd' value="x">
                        <i class="fas fa-plus"></i>
                        Agregar
                    </button>
                    &nbsp;
                    <button type="button" name="btnEdit" 
                            class="btn btn-outline-info btnAction" id="btnEdit" value="editar">
                        <i class="fas fa-pencil-alt"></i>
                        Editar
                    </button>
                    &nbsp;
                    <button type="button" name="btnDelete" id="btnDelete" 
                            class="btn btn-outline-danger btnAction" value="eliminar">
                        <i class="fas fa-trash"></i>
                        Eliminar
                    </button>
                    &nbsp;
                    <button type="button" id="btnReset" class="btn btn-outline-secondary">
                        <i class="fas fa-undo"></i>
                        Cancelar
                    </button>
                </div>
            </form>
            <br>
            <%
                }
            %>
            <!-- Example DataTables Card -->
            <div class="card mb-3">
                <div class="card-header">
                    <i class="fa fa-table"></i> Detalle de factura</div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Producto</th>
                                    <th>Cantidad</th>
                                    <th>Precio Unitario</th>
                                    <th>Subtotal</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Producto</th>
                                    <th>Cantidad</th>
                                    <th>Precio Unitario</th>
                                    <th>Subtotal</th>
                                    <th>Acciones</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <%
                                    List <DetalleFactura> listDf = daoDf.all(f.getId());
                                    for (DetalleFactura ldf : listDf)
                                    {
                                %>
                                <tr>
                                    <td><%= ldf.getProducto().getNombre() %></td>
                                    <td><%= ldf.getCantidad() %></td>
                                    <td><%= ldf.getProducto().getPrecioVenta() %></td>
                                    <td><%= ldf.getSubTotal() %></td>
                                    <td>
                                        <button type="button" data-pro="<%= ldf.getProducto().getId() %>" 
                                                data-idDf="<%= ldf.getId()%>"  
                                                data-cant="<%= ldf.getCantidad() %>" 
                                                data-pre="<%= ldf.getProducto().getPrecioVenta()%>" 
                                                data-subTot="<%= ldf.getSubTotal() %>"
                                                class="btn btn-outline-primary btn-sm col-12 select">
                                            Seleccionar
                                        </button>
                                    </td>
                                </tr>
                                <%
                                    }
                                %>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer small text-muted">
                    Última actualización: <%= LocalDateTime.now() %>
                </div>
            </div>
        </div>
        <script src="resources/js/jquery-3.3.1.js" type="text/javascript"></script>
        <script src="resources\js\popper.min.js" type="text/javascript"></script>
        <script src="resources\js\bootstrap.min.js" type="text/javascript"></script>
        <script src="resources\js\fontawesome-all.js" type="text/javascript"></script>
        <script src="resources\js\sweetalert2.all.min.js" type="text/javascript"></script>
        <script>
        <%
            if (request.getSession().getAttribute("info") != null)
            {
                String info = (String) request.getSession().getAttribute("info");
        %>
            swal('Realizado!', '<%= info %>', 'success');
            console.log('Mensaje');
        <%        
            }
            request.getSession().setAttribute("info", null);
            // Error handler
            if (request.getSession().getAttribute("Error") != null)
            {
                String error = (String) request.getSession().getAttribute("Error");
        %>
            swal({
                type: 'error',
                title: 'Error al realizar la operación',
                text: "Ocurrió el siguiente error: \n" + "<%= error %>",
            })
        <%        
            }
            request.getSession().setAttribute("Error", null);
        %>
        </script>
        <script src="resources/js/datatables.js" type="text/javascript"></script>
        <script src="resources/js/dataTables.bootstrap4.js" type="text/javascript"></script>
        <script src="resources/js/buttons.bootstrap4.js" type="text/javascript"></script>
        <script src="resources/js/momentjs.js" type="text/javascript"></script>
        <script src="resources/js/detalle.js" type="text/javascript"></script>
        <script src="resources/js/globalConfig.js" type="text/javascript"></script>
        <script>
            var datos = [
                <%
                    lsP = daoDf.findProductos();
                    for (Producto p : lsP)
                    {
                %>
                        {id: <%= p.getId() %>, pre: <%= p.getPrecioVenta() %>},
                <%
                    } 
                %>   
            ];
        </script>
    </body>
</html>
<%
    }
    else
    {
        response.sendRedirect("venta.jsp");
    }
%>