<%-- 
    Document   : reporteFechasComprendidas
    Created on : 10-03-2018, 07:41:39 PM
    Author     : Perez
--%>

<%@page import="net.sf.jasperreports.engine.*" %>
<%@page import="java.util.Map" %>
<%@page import="java.util.HashMap" %>
<%@page import="java.io.File" %>
<%@page import="com.connection.Conexion" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Reporte Fechas Comprendidas | Rango de Fechas</title>
    </head>
    <body>
        <h1 align="center">Reporte Fechas Comprendidas en un Rango de Fechas</h1>
        <div align="center"> 
            <%
                Conexion c = new Conexion();
                c.conectar();
                File reporte = new File(application.getRealPath("reports/facturasRangoFechas.jasper"));
                Map params = new HashMap();
                String f1 = request.getParameter("f1");
                String f2 = request.getParameter("f2");
                params.put("f1", f1);
                params.put("f2", f2);
                byte[] bytes = JasperRunManager.runReportToPdf(reporte.getPath(), params, c.getConn());
                response.setContentType("application/pdf");
                response.setContentLength(bytes.length);
                ServletOutputStream output = response.getOutputStream();
                output.write(bytes,0,bytes.length);
                output.flush();
                output.close();
            %>
        </div>
    </body>
</html>
