package com.connection;

import java.sql.*;
import javax.swing.JOptionPane;

/**
 * Nombre de la clase: Conexion
 * Fecha: 13/09/2018
 * Version: 1.0
 * Copyright ITCA-FEPADE
 * @author Juan Pablo Elias Hernández
 */
public class Conexion
{
    private Connection conn = null;

    public Conexion()
    {
        // Nada que hacer
    }
    
    public Connection getConn() {
        return conn;
    }

    public void setConn(Connection conn) {
        this.conn = conn;
    }
    
    public void conectar()
    {
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            conn = (Connection) DriverManager.getConnection
            (
                    "jdbc:mysql://localhost:3306/tareaevaluada2", "root", ""
            );
        } 
        catch (SQLException | ClassNotFoundException e) 
        {
            JOptionPane.showMessageDialog(null, "Error al conectar: "
                    + e.toString(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void desconectar()
    {
        try
        {
            if (conn != null && !conn.isClosed())
            {
                conn.close();
            }
        }
        catch (SQLException e)
        {
            JOptionPane.showMessageDialog(null, "Error al desconectar: " 
                    + e.toString(), "Error", 0);
        }
    }

}
