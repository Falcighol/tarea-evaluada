package com.models;

/**
 * Nombre de la clase Factura
 * Fecha 24/09/18
 * Version 1.0
 * Copyright ITCA-FEPADE
 * @author Juan Pablo Elias Hernández
 */
public class Factura
{
    private Integer id;
    private String numFactura;
    private Cliente cliente;
    private Vendedor vendedor;
    private Double total;
    private String fecha;

    public Factura() {
    }

    public Factura(Integer id, String numFactura, Cliente cliente, Vendedor vendedor, Double total, String fecha) {
        this.id = id;
        this.numFactura = numFactura;
        this.cliente = cliente;
        this.vendedor = vendedor;
        this.total = total;
        this.fecha = fecha;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumFactura() {
        return numFactura;
    }

    public void setNumFactura(String numFactura) {
        this.numFactura = numFactura;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Vendedor getVendedor() {
        return vendedor;
    }

    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }
    
}
