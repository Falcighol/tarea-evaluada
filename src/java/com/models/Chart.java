package com.models;

import com.connection.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Juan Pablo Elias Hernández
 */
public class Chart extends Conexion
{
    public ArrayList getProductName() throws Exception
    {
        ArrayList ls = new ArrayList();
        ResultSet res;
        try
        {
            this.conectar();
            String sql = "SELECT nombre, id FROM Producto ORDER BY id DESC LIMIT 5;";
            PreparedStatement pst = this.getConn().prepareStatement(sql);
            res= pst.executeQuery();
            while(res.next())
            {
               ls.add("'" + res.getString(1) + "'");
            }
        }
        catch (Exception e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
        return ls;
    }
    
    public ArrayList getProductStock() throws Exception
    {
        ArrayList ls = new ArrayList();
        ResultSet res;
        try
        {
            this.conectar();
            String sql = "SELECT stockAct, id FROM Producto ORDER BY id DESC LIMIT 5;";
            PreparedStatement pst = this.getConn().prepareStatement(sql);
            res= pst.executeQuery();
            while(res.next())
            {
               ls.add(res.getInt(1));
            }
        }
        catch (Exception e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
        return ls;
    }
    
    public ArrayList getColors() throws Exception
    {
        ArrayList ls = new ArrayList();
        ResultSet res;
        try
        {
            this.conectar();
            String sql = "SELECT stockAct, id FROM Producto ORDER BY id DESC;";
            PreparedStatement pst = this.getConn().prepareStatement(sql);
            res= pst.executeQuery();
            while(res.next())
            {
               ls.add("'rgba(54, 162, 235, 0.2)'");
            }
        }
        catch (Exception e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
        return ls;
    }
    
    public ArrayList getBorderColors() throws Exception
    {
        ArrayList ls = new ArrayList();
        ResultSet res;
        try
        {
            this.conectar();
            String sql = "SELECT stockAct, id FROM Producto ORDER BY id DESC;";
            PreparedStatement pst = this.getConn().prepareStatement(sql);
            res= pst.executeQuery();
            while(res.next())
            {
               ls.add("'rgba(54, 162, 235, 1)'");
            }
        }
        catch (Exception e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
        return ls;
    }
    
    public ArrayList getSellerName() throws Exception
    {
        ArrayList ls = new ArrayList();
        ResultSet res;
        try
        {
            this.conectar();
            String sql = "SELECT V.nombre AS Nombre, COUNT(F.idVendedor) "
                    + "AS CantVentas FROM Factura F INNER JOIN Vendedor V ON "
                    + "F.idVendedor = V.id GROUP BY F.idVendedor ORDER BY "
                    + "CantVentas DESC LIMIT 5";
            PreparedStatement pst = this.getConn().prepareStatement(sql);
            res= pst.executeQuery();
            while(res.next())
            {
               ls.add("'" + res.getString(1) + "'");
            }
        }
        catch (Exception e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
        return ls;
    }
    
    public ArrayList getSellerCant() throws Exception
    {
        ArrayList ls = new ArrayList();
        ResultSet res;
        try
        {
            this.conectar();
            String sql = "SELECT V.nombre AS Nombre, COUNT(F.idVendedor) "
                    + "AS CantVentas FROM Factura F INNER JOIN Vendedor V ON "
                    + "F.idVendedor = V.id GROUP BY F.idVendedor ORDER BY "
                    + "CantVentas DESC LIMIT 5";
            PreparedStatement pst = this.getConn().prepareStatement(sql);
            res= pst.executeQuery();
            while(res.next())
            {
               ls.add("'" + res.getString(2) + "'");
            }
        }
        catch (Exception e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
        return ls;
    }
}
