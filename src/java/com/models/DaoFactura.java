package com.models;

import com.connection.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Nombre de la clase DaoFactura
 * Fecha 1/10/2018
 * Version 1.0
 * Copyright ITCA-FEPADE
 * @author Juan Pablo Elias Hernández
 */
public class DaoFactura extends Conexion
{
    //<editor-fold defaultstate="collapsed" desc="Métodos de la clase">
    public List all() throws Exception
    {
        List facturas = new ArrayList();
        ResultSet res = null;
        try
        {
            this.conectar();
            String sql = "SELECT F.*, C.nombre AS nomC, V.nombre AS nomV FROM "
                    + "Factura F INNER JOIN Cliente C ON F.idCliente = C.id "
                    + "INNER JOIN Vendedor V ON F.idVendedor = V.id ORDER BY id DESC;";
            PreparedStatement pre = getConn().prepareStatement(sql);
            res = pre.executeQuery();
            while (res.next())
            {
                Factura f = new Factura();
                Cliente c = new Cliente();
                Vendedor v = new Vendedor();
                f.setId(res.getInt(1));
                f.setNumFactura(res.getString(2));
                c.setId(res.getInt(3));
                v.setId(res.getInt(4));
                f.setTotal(res.getDouble(5));
                f.setFecha(res.getString(6));
                c.setNombre(res.getString(7));
                v.setNombre(res.getString(8));
                f.setCliente(c);
                f.setVendedor(v);
                facturas.add(f);
            }
        }
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            desconectar();
        }
        return facturas;
    }
    
    public Factura findOne(String num) throws Exception
    {
        List factura = new ArrayList();
        ResultSet res = null;
        Factura f = new Factura();
        try
        {
            this.conectar();
            String sql = "SELECT F.*, C.nombre AS nomC, V.nombre AS nomV FROM "
                    + "Factura F INNER JOIN Cliente C ON F.idCliente = C.id "
                    + "INNER JOIN Vendedor V ON F.idVendedor = V.id "
                    + "WHERE numFactura = ?;";
            PreparedStatement pre = getConn().prepareStatement(sql);
            pre.setString(1, num);
            res = pre.executeQuery();
            if (res.next())
            {
                f = new Factura();
                Cliente c = new Cliente();
                Vendedor v = new Vendedor();
                f.setId(res.getInt(1));
                f.setNumFactura(res.getString(2));
                c.setId(res.getInt(3));
                v.setId(res.getInt(4));
                f.setTotal(res.getDouble(5));
                f.setFecha(res.getString(6));
                c.setNombre(res.getString(7));
                v.setNombre(res.getString(8));
                f.setCliente(c);
                f.setVendedor(v);
            }
        }
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            desconectar();
        }
        return f;
    }
    
    public int add(Factura f) throws Exception
    {
        int estado = 0;
        try
        {
            conectar();
            String sql = "INSERT INTO Factura VALUES(null, ?, ?, ?, ?, ?);";
            PreparedStatement pre = getConn().prepareStatement(sql);
            pre.setString(1, f.getNumFactura());
            pre.setInt(2, f.getCliente().getId());
            pre.setInt(3, f.getVendedor().getId());
            pre.setDouble(4, f.getTotal());
            pre.setString(5, f.getFecha());
            pre.executeUpdate();
        } 
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            desconectar();
        }
        return estado;
    }
    
    public int edit(Factura f) throws Exception
    {
        int estado = 0;
        try
        {
            conectar();
            String sql = "UPDATE Factura SET total = (SELECT SUM(subTotal) "
                    + "AS Total FROM DetalleFactura WHERE idFactura = ?) WHERE id = ?;";
            PreparedStatement pre = getConn().prepareStatement(sql);
            pre.setInt(1, f.getId());
            pre.setInt(2, f.getId());
            pre.executeUpdate();
        } 
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            desconectar();
        }
        return estado;
    }
    
    public int delete(Factura f) throws Exception
    {
        int estado = 0;
        try
        {
            conectar();
            String sql = "DELETE FROM Factura WHERE id = ?;";
            PreparedStatement pre = getConn().prepareStatement(sql);
            pre.setInt(1, f.getId());
            pre.executeUpdate();
        } 
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            desconectar();
        }
        return estado;
    }
    
    public List allClientes() throws Exception
    {
        List clientes = new ArrayList();
        ResultSet res = null;
        try
        {
            this.conectar();
            String sql = "SELECT * FROM Cliente";
            PreparedStatement pre = getConn().prepareStatement(sql);
            res = pre.executeQuery();
            while (res.next())
            {
                Cliente c = new Cliente();
                c.setId(res.getInt(1));
                c.setNombre(res.getString(2));
                c.setApellidos(res.getString(3));
                c.setDireccion(res.getString(4));
                clientes.add(c);
            }
        }
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            desconectar();
        }
        return clientes;
    }
    
    public List allVendedores() throws Exception
    {
        List vendedores = new ArrayList();
        ResultSet res = null;
        try
        {
            this.conectar();
            String sql = "SELECT * FROM Vendedor";
            PreparedStatement pre = getConn().prepareStatement(sql);
            res = pre.executeQuery();
            while (res.next())
            {
                Vendedor v = new Vendedor();
                v.setId(res.getInt(1));
                v.setDui(res.getString(2));
                v.setNombre(res.getString(3));
                v.setApellidos(res.getString(4));
                v.setDireccion(res.getString(5));
                v.setTelOficina(res.getString(6));
                v.setTelMovil(res.getString(7));
                vendedores.add(v);
            }
        }
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            desconectar();
        }
        return vendedores;
    }
    //</editor-fold>
}
