
package com.models;
import java.util.*;
import com.modelS.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import com.connection.Conexion;
/**
 * Nombre de la clase: DaoProducto
 * Fecha: 24/09/2018
 * Version: 1.0
 * Copyrigth: Alvaro Perez 
 * Autor : Perez
 */
public class DaoProducto extends Conexion
{
    public List <Producto> mostrar() throws Exception
    {
        List<Producto> lista = new ArrayList();
        ResultSet res;
        try
        {
            this.conectar();
            String sql = "SELECT * FROM Producto ORDER BY id DESC;";
            PreparedStatement pst = this.getConn().prepareStatement(sql);
            res= pst.executeQuery();
            while(res.next())
            {
               Producto p = new Producto();
               p.setId(res.getInt(1));
               p.setNombre(res.getString(2));
               p.setPrecioVenta(res.getDouble(3));
               p.setStockMin(res.getInt(4));
               p.setStockAct(res.getInt(5));
               p.setCodigoBarra(res.getString(6));
               lista.add(p);
            }
        }
        catch (Exception e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
        return lista;
    }
    
    public void agregar(Producto produ) throws Exception 
    {
        try 
        {
            this.conectar();
            String sql = "insert into producto values(null,?,?,?,?,?);";
            PreparedStatement pst = this.getConn().prepareStatement(sql);
            pst.setString(1, produ.getNombre());
            pst.setDouble(2, produ.getPrecioVenta());
            pst.setInt(3, produ.getStockMin());
            pst.setInt(4, produ.getStockAct());
            pst.setString(5, produ.getCodigoBarra());
            
            pst.executeUpdate();
        } 
        catch (Exception e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
    }
    
    public void modificar(Producto p) throws Exception
    {
        try 
        {
            this.conectar();
            String sql = "UPDATE Producto SET nombre = ?, precioVenta = ?, "
                    + "stockMin = ?, stockAct = ?, codigoBarra = ? WHERE id = ?;";
            PreparedStatement pst = this.getConn().prepareStatement(sql);
            pst.setString(1, p.getNombre());
            pst.setDouble(2, p.getPrecioVenta());
            pst.setInt(3, p.getStockMin());
            pst.setInt(4, p.getStockAct());
            pst.setString(5, p.getCodigoBarra());
            pst.setInt(6, p.getId());
            
            pst.executeUpdate();
        }
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
    }
    
    public void eliminar(Producto p) throws Exception
    {
        try
        {
            this.conectar();
            String sql = "delete from producto where id=?";
            PreparedStatement pst = this.getConn().prepareStatement(sql);
            pst.setInt(1, p.getId());
                    
            pst.executeUpdate();
        } catch (Exception e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
    }
}
