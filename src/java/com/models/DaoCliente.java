package com.models;

import com.connection.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kevin Lovos
 */
public class DaoCliente extends Conexion{
    public List <Cliente> mostrarCliente () throws Exception
    {
        List <Cliente> Lista = new ArrayList(); 
        ResultSet res;
        try 
        {
            this.conectar();
            String sql="SELECT * FROM Cliente ORDER BY id DESC;";
            PreparedStatement pre = this.getConn().prepareStatement(sql);
            res = pre.executeQuery();
            while(res.next())
            {
                Cliente cli = new Cliente();
                cli.setId(res.getInt(1));
                cli.setNombre(res.getString(2));
                cli.setApellidos(res.getString(3));
                cli.setDireccion(res.getString(4));
                Lista.add(cli);
            }
           
        } 
        catch (Exception e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
        return Lista;
    }
    
    public void addCliente(Cliente cli) throws Exception
    {
        try 
        {
           this.conectar();
           String sql="insert into Cliente values(null, ?, ?, ?)";
           PreparedStatement pre = this.getConn().prepareStatement(sql);
           pre.setString(1, cli.getNombre());
           pre.setString(2, cli.getApellidos());
           pre.setString(3,cli.getDireccion());
           pre.executeUpdate();
        } 
        catch (Exception e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
    }
    
    public void editCliente(Cliente cli) throws Exception
    {
        try 
        {
            this.conectar();
            String sql="update Cliente set nombre = ?,apellidos=?,direccion=? where id = ?";
            PreparedStatement pre = this.getConn().prepareStatement(sql);
            pre.setString(1, cli.getNombre());
            pre.setString(2, cli.getApellidos());
            pre.setString(3,cli.getDireccion());
            pre.setInt(4, cli.getId());
            pre.executeUpdate();
        } 
        catch (Exception e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
    }
    
    public void deleteCliente(Cliente cli) throws Exception
    {
        try 
        {
            this.conectar();
            String sql="DELETE FROM Cliente WHERE id = ?";
            PreparedStatement pre = this.getConn().prepareStatement(sql);
            pre.setInt(1, cli.getId());
            pre.executeUpdate();
        } 
        catch (Exception e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
    }
}
