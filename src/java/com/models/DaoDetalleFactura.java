package com.models;

import com.connection.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Nombre de la clase DaoDetalleFactura
 * Fecha 1/10/2018
 * Version 1.0
 * Copyright ITCA-FEPADE
 * @author Juan Pablo Elias Hernández
 */
public class DaoDetalleFactura extends Conexion
{
    //<editor-fold defaultstate="collapsed" desc="Métodos de la clase">
    public List all(Integer id) throws Exception
    {
        List facturas = new ArrayList();
        ResultSet res = null;
        try
        {
            this.conectar();
            String sql = "SELECT DF.*, P.nombre AS nomPro, P.precioVenta AS prePro "
                    + "FROM DetalleFactura DF INNER JOIN Producto P ON "
                    + "DF.idProducto = P.id WHERE DF.idFactura = ? ORDER BY id DESC;";
            PreparedStatement pre = getConn().prepareStatement(sql);
            pre.setInt(1, id);
            res = pre.executeQuery();
            while (res.next())
            {
                DetalleFactura df = new DetalleFactura();
                Factura f = new Factura();
                Producto p = new Producto();
                df.setId(res.getInt(1));
                f.setId(res.getInt(2));
                p.setId(res.getInt(3));
                df.setCantidad(res.getInt(4));
                df.setSubTotal(res.getDouble(5));
                p.setNombre(res.getString(6));
                p.setPrecioVenta(res.getDouble(7));
                df.setFactura(f);
                df.setProducto(p);
                facturas.add(df);
            }
        }
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            desconectar();
        }
        return facturas;
    }
    
    public int add(DetalleFactura df) throws Exception
    {
        int estado = 0;
        try
        {
            conectar();
            String sql = "INSERT INTO DetalleFactura VALUES(null, ?, ?, ?, ?);";
            PreparedStatement pre = getConn().prepareStatement(sql);
            pre.setInt(1, df.getFactura().getId());
            pre.setInt(2, df.getProducto().getId());
            pre.setInt(3, df.getCantidad());
            pre.setDouble(4, df.getSubTotal());
            pre.executeUpdate();
        } 
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            desconectar();
        }
        return estado;
    }
    
    public int edit(DetalleFactura df) throws Exception
    {
        int estado = 0;
        try
        {
            conectar();
            String sql = "UPDATE DetalleFactura SET idProducto = ?, "
                    + "cantidad = ?, subTotal = ? WHERE id = ?;";
            PreparedStatement pre = getConn().prepareStatement(sql);
            pre.setInt(1, df.getProducto().getId());
            pre.setInt(2, df.getCantidad());
            pre.setDouble(3, df.getSubTotal());
            pre.setInt(4, df.getId());
            pre.executeUpdate();
        } 
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            desconectar();
        }
        return estado;
    }
    
    public int delete(DetalleFactura df) throws Exception
    {
        int estado = 0;
        try
        {
            conectar();
            String sql = "DELETE FROM DetalleFactura WHERE id = ?";
            PreparedStatement pre = getConn().prepareStatement(sql);
            pre.setInt(1, df.getId());
            pre.executeUpdate();
        } 
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            desconectar();
        }
        return estado;
    }
    
    public List findProductos() throws Exception
    {
        List productos = new ArrayList();
        ResultSet res = null;
        try
        {
            this.conectar();
            String sql = "SELECT * FROM Producto;";
            PreparedStatement pre = getConn().prepareStatement(sql);
            res = pre.executeQuery();
            while (res.next())
            {
                Producto p = new Producto();
                p.setId(res.getInt(1));
                p.setNombre(res.getString(2));
                p.setPrecioVenta(res.getDouble(3));
                productos.add(p);
            }
        }
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            desconectar();
        }
        return productos;
    }
    
    public int updateStock(Integer idP) throws Exception
    {
        int estado = 0;
        try
        {
            conectar();
            String sql = "UPDATE Producto SET stockAct = stockAct - " +
            "(SELECT SUM(cantidad) FROM DetalleFactura " +
            "WHERE idProducto = ?) WHERE id = ?;";
            PreparedStatement pre = getConn().prepareStatement(sql);
            pre.setInt(1, idP);
            pre.setInt(2, idP);
            pre.executeUpdate();
        } 
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            desconectar();
        }
        return estado;
    }
    //</editor-fold>
}
