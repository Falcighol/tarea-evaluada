package com.models;

/**
 * Nombre de la clase: Producto
 * Fecha: 24/09/2018
 * Version: 1.0
 * Copyrigth: Alvaro Perez 
 * Autor : Perez
 */
public class Producto
{
    private int id;
    private String nombre;
    private double precioVenta;
    private int stockMin;
    private int stockAct;
    private String codigoBarra;

    public Producto()
    {
        
    }

    public Producto(int id, String nombreProducto, double precioVenta, int stockMinimo, int stockActual, String codigoBarra) {
        this.id = id;
        this.nombre = nombreProducto;
        this.precioVenta = precioVenta;
        this.stockMin = stockMinimo;
        this.stockAct = stockActual;
        this.codigoBarra = codigoBarra;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getPrecioVenta() {
        return precioVenta;
    }

    public void setPrecioVenta(double precioVenta) {
        this.precioVenta = precioVenta;
    }

    public int getStockMin() {
        return stockMin;
    }

    public void setStockMin(int stockMin) {
        this.stockMin = stockMin;
    }

    public int getStockAct() {
        return stockAct;
    }

    public void setStockAct(int stockAct) {
        this.stockAct = stockAct;
    }

    public String getCodigoBarra() {
        return codigoBarra;
    }

    public void setCodigoBarra(String codigoBarra) {
        this.codigoBarra = codigoBarra;
    }
    
}
