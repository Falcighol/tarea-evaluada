
package com.models;

import com.connection.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import com.models.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Nombre de la clase: DaoVendedor
 * Fecha: 24/09/2018
 * Version: 1.0
 * Copyrigth: Alvaro Perez 
 * Autor : Perez
 */
public class DaoVendedor  extends Conexion
{
    
    public List all() throws Exception
    {
        List vendedores = new ArrayList();
        ResultSet res;
        try 
        {
            this.conectar();
            String sql = "SELECT * FROM Vendedor ORDER BY id DESC;";
            PreparedStatement pst = this.getConn().prepareStatement(sql);
            res = pst.executeQuery();
            while(res.next())
            {
                Vendedor v = new Vendedor();
                v.setId(res.getInt(1));
                v.setDui(res.getString(2));
                v.setNombre(res.getString(3));
                v.setApellidos(res.getString(4));
                v.setDireccion(res.getString(5));
                v.setTelOficina(res.getString(6));
                v.setTelMovil(res.getString(7));
                vendedores.add(v);
            }
        } 
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
        return vendedores;
    }
    
    public void add(Vendedor v) throws Exception 
    {
        try 
        {
            this.conectar();
            String sql = "INSERT INTO Vendedor VALUES(NULL, ?, ?, ?, ?, ?, ?);";
            PreparedStatement pst = this.getConn().prepareStatement(sql);
            pst.setString(1, v.getDui());
            pst.setString(2, v.getNombre());
            pst.setString(3, v.getApellidos());
            pst.setString(4, v.getDireccion());
            pst.setString(5, v.getTelOficina());
            pst.setString(6, v.getTelMovil());
            pst.executeUpdate();
        }
        catch (Exception e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
    }
    
    public void edit(Vendedor v) throws Exception
    {
        try 
        {
            this.conectar();
            String sql = "UPDATE Vendedor SET dui = ?, nombre = ?, apellidos = ?, "
                    + "direccion = ?, telOficina = ?, telMovil = ? WHERE id = ?;";
            PreparedStatement pst = this.getConn().prepareStatement(sql);
            pst.setString(1, v.getDui());
            pst.setString(2, v.getNombre());
            pst.setString(3, v.getApellidos());
            pst.setString(4, v.getDireccion());
            pst.setString(5, v.getTelOficina());
            pst.setString(6, v.getTelMovil());
            pst.setInt(7, v.getId());
            pst.executeUpdate();
        } 
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
    }
    
    public void delete(Vendedor v) throws Exception
    {
        try
        {
            this.conectar();
            String sql = "DELETE FROM Vendedor WHERE id = ?;";
            PreparedStatement pst = this.getConn().prepareStatement(sql);
            pst.setInt(1, v.getId());
            pst.executeUpdate();
        }
        catch (Exception e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
    }
}
