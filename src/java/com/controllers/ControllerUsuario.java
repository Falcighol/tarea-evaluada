package com.controllers;

import com.models.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Nombre de servlet: ControllerUsuario
 * Fecha: 3/10/2018
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * @author Kevin Lovo
 */
public class ControllerUsuario extends HttpServlet
{

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        DaoUsuario daoU = new DaoUsuario();
        Usuario u = new Usuario();
        String url = "usuario.jsp", info = "";
        try
        {
            if(request.getParameter("btnAdd") != null)
            {
                u.setTu(Integer.parseInt(request.getParameter("tu")));
                u.setNombre(request.getParameter("nom"));
                u.setPass(request.getParameter("pass"));
                u.setEstado(Integer.parseInt(request.getParameter("est")));
                daoU.add(u);
                info = "Datos guardados correctamente en la base de datos.";
            }
            else if(request.getParameter("action") != null
                    && "editar".equals(request.getParameter("action")))
            {
                
                u.setId(Integer.parseInt(request.getParameter("id")));
                u.setTu(Integer.parseInt(request.getParameter("tu")));
                u.setNombre(request.getParameter("nom"));
                u.setPass(request.getParameter("pass"));
                u.setEstado(Integer.parseInt(request.getParameter("est")));
                daoU.edit(u);
                info = "Datos editados correctamente en la base de datos.";
            }
            else if(request.getParameter("action") != null
                    && "eliminar".equals(request.getParameter("action")))
            {
                
                u.setId(Integer.parseInt(request.getParameter("id")));
                daoU.delete(u);
                info = "Datos eliminados correctamente de la base de datos.";
            }
            else if (request.getParameter("btnLogin") != null)
            {
                u.setNombre(request.getParameter("user"));
                u.setPass(request.getParameter("pass"));
                Integer tu = daoU.validateUser(u);
                if (tu != null && tu != 0)
                {
                    request.getSession().setAttribute("tu", tu);
                    request.getSession().setAttribute("user", u.getNombre());
                }
                else
                {
                    info = "Datos de ingreso incorrectos, intentelo de nuevo.";
                }
                url = "login.jsp";
            }
            else if (request.getParameter("close") != null)
            {
                HttpSession ss = request.getSession();
                ss.setAttribute("close", true);
                url = "login.jsp";
            }
            request.getSession().setAttribute("info", info);
        }
        catch (Exception e)
        {
            request.getSession().setAttribute("Error", "el nombre de usuario ingresado ya existe.");
            // out.print(e.toString());
        }
        response.sendRedirect(url);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
