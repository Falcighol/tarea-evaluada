package com.controllers;

import com.models.*;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Nombre de servlet: ControllerFactura
 * Fecha: 3/10/2018
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * @author Juan Pablo Elias Hernández
 */
public class ControllerFactura extends HttpServlet
{
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        DaoFactura daoF = new DaoFactura();
        Factura f = new Factura();
        Cliente c = new Cliente();
        Vendedor v = new Vendedor();
        try
        {
            f.setId(Integer.parseInt(request.getParameter("fId")));
            f.setNumFactura(request.getParameter("num"));
            c.setId(Integer.parseInt(request.getParameter("cId")));
            v.setId(Integer.parseInt(request.getParameter("vId")));
            f.setCliente(c);
            f.setVendedor(v);
            f.setTotal(0.0);
            f.setFecha(request.getParameter("fecha"));
            if (request.getParameter("btnAdd") != null)
            {
                daoF.add(f);
            }
            request.getSession().setAttribute("cF", f.getNumFactura());
        } 
        catch (Exception e)
        {
            request.getSession().setAttribute("Error", e.toString());
        }
        response.sendRedirect("detalle.jsp");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
