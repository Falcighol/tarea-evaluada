package com.controllers;

import com.models.*;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Nombre del servlet: ProcesarProducto
 * Fecha: 24/09/2018
 * Version: 1.0
 * Copyrigth: Alvaro Perez 
 * Autor : Perez
 */
public class ControllerVendedor extends HttpServlet
{
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        Vendedor v = new Vendedor();
        DaoVendedor daoV = new DaoVendedor();
        String msj ="";
        RequestDispatcher rd = null;
        try
        {
            v.setId(Integer.parseInt(request.getParameter("txtId")));
            v.setDui(request.getParameter("txtDui"));
            v.setNombre(request.getParameter("txtNombre"));
            v.setApellidos(request.getParameter("txtApellidos"));
            v.setDireccion(request.getParameter("txtDireccion"));
            v.setTelOficina(request.getParameter("txtTelOficina"));
            v.setTelMovil(request.getParameter("txtTelMovil"));
            if(request.getParameter("btnAdd") != null)
            {
                daoV.add(v);
                msj = "Datos guardados correctamente en la base de datos.";
            }
            else if(request.getParameter("action") != null
                    && "editar".equals(request.getParameter("action")))
            {
                daoV.edit(v);
                msj = "Datos editados correctamente en la base de datos.";
            }
            else if(request.getParameter("action") != null
                    && "eliminar".equals(request.getParameter("action")))
            {
                daoV.delete(v);
                msj = "Datos eliminados correctamente de la base de datos.";
            }
            request.getSession().setAttribute("info", msj);
        }
        catch(Exception e)
        {
            request.getSession().setAttribute("Error", "El DUI debe ser unico.");
        }
        response.sendRedirect("vendedor.jsp");
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
