package com.controllers;

import com.google.gson.Gson;
import com.models.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Nombre del servlet: ControllerDetalleFactura
 * Fecha: 3/10/2018
 * Version: 1.0
 * Copyright: ITCA-FEPADE
 * @author Juan Pablo Elias Hernández
 */
public class ControllerDetalleFactura extends HttpServlet
{
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        DaoDetalleFactura daoDf = new DaoDetalleFactura();
        DaoFactura daoF = new DaoFactura();
        DetalleFactura df = new DetalleFactura();
        Producto p = new Producto();
        Factura f = new Factura();
        String info = null;
        try
        {
            df.setId(Integer.parseInt(request.getParameter("dfId")));
            f.setId(Integer.parseInt(request.getParameter("fId")));
            f.setNumFactura(request.getParameter("num"));
            p.setId(Integer.parseInt(request.getParameter("pro")));
            df.setCantidad(Integer.parseInt(request.getParameter("cant")));
            df.setSubTotal(Double.parseDouble(request.getParameter("sub")));
            df.setFactura(f);
            df.setProducto(p);
            if (request.getParameter("btnAdd") != null)
            {
                daoDf.add(df);
                info = "Datos guardados correctamente en la base de datos.";
            }
            else if (request.getParameter("action") != null
                    && "editar".equals(request.getParameter("action")))
            {
                daoDf.edit(df);
                info = "Datos editados correctamente en la base de datos.";
            }
            else if (request.getParameter("action") != null
                    && "eliminar".equals(request.getParameter("action")))
            {
                daoDf.delete(df);
                info = "Datos eliminados correctamente de la base de datos.";
            }
            daoF.edit(f);
            daoDf.updateStock(Integer.parseInt(request.getParameter("pro")));
            request.getSession().setAttribute("info", info);
        } 
        catch (Exception e)
        {
            request.getSession().setAttribute("Error", e.toString());
        }
        response.sendRedirect("detalle.jsp?cF=" + df.getFactura().getNumFactura());
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
