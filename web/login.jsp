<%-- 
    Document   : producto
    Created on : 09-24-2018, 01:32:12 PM
    Author     : Juan Pablo Elias Hernandez
--%>

<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.models.*;" %>
<%@page session="true" %>

<%
    HttpSession ss = request.getSession();
    if (ss.getAttribute("close") != null)
    {
        Boolean close = (Boolean) ss.getAttribute("close");
        if (close)
        {
            ss.invalidate();
        }
    }
    else if (ss.getAttribute("tu") != null || ss.getAttribute("user") != null)
    {
        Integer idTu = (Integer) ss.getAttribute("tu");
        if (idTu == 1 || idTu == 2 || idTu == 3)
        {
            response.sendRedirect("index.jsp");
        }
    }
%>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Login de la aplicacion">
    <meta name="author" content="Juan Pablo Elias Hernandez">
    <title>Login</title>
    <!-- Bootstrap core CSS-->
    <link href="resources/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
</head>
<body class="bg-dark">
    <style>
        #alert-container {
            position: relative;
        }
        
        .alert { 
            position: absolute;
            display: none; 
        }
        
        @media (min-width: 576px) {
            .card-columns {
                column-count: 1;
            }
        }

        @media (min-width: 768px) {
            .card-columns {
                column-count: 2;
            }
        }

        @media (min-width: 1200px) {
            .card-columns {
                column-count: 2;
            }
        }
        .card-login {
            max-width: 25rem;
        }
    </style>
    <div id="alert-container" class="col-12">
        <div class="alert alert-danger alert-dismissible fade show" role="alert" style="margin-top: 1.5rem;">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
    <div class="container">
        <div class="card card-login mx-auto mt-5">
            <div class="card-header">
                <h4><i class="fa fa-sign-in-alt"></i>&nbsp;Iniciar sesión</h4>
            </div>
            <div class="card-body">
                <form id="frm" action="controllerUsuario" method="post" class="needs-validation" novalidate>
                    <div class="form-group" align="center">
                        <img src="resources/img/lock-icon.png" alt="Login" width="40%"/>
                    </div>
                    <div class="form-group">
                        <label for="user">Usuario:</label>
                        <input class="form-control" id="user" name="user" type="text" 
                               aria-describedby="userHelp" placeholder="Usuario" required>
                        <div class="invalid-feedback">
                            Por favor, ingrese su usuario.
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="pass">Contraseña:</label>
                        <input class="form-control" id="pass" name="pass" type="password" 
                               placeholder="Contraseña" required>
                        <div class="invalid-feedback">
                            Por favor, ingrese su contraseña.
                        </div>
                    </div>
                    <button type="submit" id="btnLogin" name="btnLogin" value="x" class="btn btn-dark btn-block">
                        <i class="fa fa-sign-in-alt"></i>&nbsp;
                        Ingresar
                    </button>
                </form>
            </div>
        </div>
    </div>
    <script src="resources/js/jquery-3.3.1.js" type="text/javascript"></script>
    <script src="resources/js/popper.min.js" type="text/javascript"></script>
    <script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="resources/js/fontawesome-all.js" type="text/javascript"></script>
    <script src="resources/js/login.js" type="text/javascript"></script>
    <script>
    <%
        if (request.getSession().getAttribute("info") != null)
        {
            String info = (String) request.getSession().getAttribute("info");
    %>
        showError('<%= info %>');
    <%
        }
        request.getSession().setAttribute("info", null);
    %>
    </script>
</body>

</html>
