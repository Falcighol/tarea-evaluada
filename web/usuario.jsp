<%-- 
    Document   : usuario
    Created on : 8/10/2018, 10:22:23 AM
    Author     : Juan Pablo Elias Hernández
--%>

<%@page import="java.time.LocalDateTime"%>
<%@page import="java.time.format.DateTimeFormatter"%>
<%@page import="java.util.List"%>
<%@page import="com.models.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<% 
    HttpSession ss = request.getSession();
    Integer idTu = -1;
    String user = "";
    if (ss.getAttribute("tu") != null || ss.getAttribute("user") != null)
    {
        idTu = (Integer) ss.getAttribute("tu");
        user = (String) ss.getAttribute("user");
        if (idTu != 1)
        {
            response.sendRedirect("index.jsp");
        }
    }
    else
    {
        response.sendRedirect("login.jsp");
    }
    Usuario cli = new Usuario();
    DaoUsuario daoC = new DaoUsuario();
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" type="image/png" href="resources/img/Icon-shop.png" sizes="64x64">
        <link href="resources\css\bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="resources/css/sweetalert2.min.css" rel="stylesheet" type="text/css">
        <link href="resources/css/datatables.css" rel="stylesheet" type="text/css"/>
        <link href="resources/css/dataTables.bootstrap4.css"/>
        <link href="resources/css/buttons.bootstrap4.css" rel="stylesheet" type="text/css"/>
        <title>Usuarios</title>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="#">
                <i class="fas fa-store"></i>
                Tienda
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="index.jsp">
                            <i class="fas fa-home"></i>
                            Inicio <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="producto.jsp">
                            <i class="fas fa-shopping-basket"></i>
                            Productos
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="vendedor.jsp">
                            <i class="fas fa-id-card-alt"></i>
                            Vendedores
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="cliente.jsp">
                            <i class="fas fa-users"></i>
                            Clientes
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="venta.jsp">
                            <i class="fas fa-receipt"></i>
                            Venta
                        </a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="usuario.jsp">
                            <i class="fas fa-user"></i>
                            Usuarios
                        </a>
                    </li>
                </ul>
                <form action="controllerUsuario" id="closeFrm" method="post" class="form-inline my-2 my-lg-0">
                    <input type="text" name="close" value="close" style="display: none;">
                    <button class="btn btn-outline-light btn-sm my-2 my-sm-0" 
                            name="close" value="close" type="button" id="closeSession">
                        <i class="fas fa-sign-out-alt"></i>
                        Cerrar sesion
                    </button>
                </form>
            </div>
        </nav>
        <div class="container">
            <br>
            <br>
            <h1 align="center" class="text-dark">
                <i class="fas fa-users"></i>
                Usuarios
            </h1>
            <br>
            <%
                if (idTu == 1)
                {
            %>
            <form action="controllerUsuario" id="frm" method="post" class="needs-validation" novalidate>
                <div class="form-row">
                    <div class="form-group col-sm-12" style="display: none;">
                        <label for="id">ID:</label>
                        <input value="0" type="number" id="id" class="form-control" name="id" required readonly>
                        <input type="text" name="action" id="action"/>
                    </div>
                    <div class="form-group col-sm-12 col-md-3">
                        <label for="nom">Usuario:</label>
                        <input type="text" placeholder="Usuario" id="nom" class="form-control" name="nom" required>
                        <div class="invalid-feedback">
                            Por favor, introduzca el nombre del usuario.
                        </div>
                    </div>
                    <div class="form-group col-sm-12 col-md-3">
                        <label for="pass">Contraseña:</label>
                        <input type="password" placeholder="Contraseña" id="pass" class="form-control" name="pass" required>
                        <div class="invalid-feedback">
                            Por favor, introduzca la contraseña del usuario.
                        </div>
                    </div>
                    <div class="form-group col-sm-12 col-md-3">
                        <label for="est">Estado:</label>
                        <select  id="est" class="form-control" name="est" required>
                            <option value="">-- Seleccionar --</option>
                            <option value="1">Activado</option>
                            <option value="2">Desactivado</option>
                        </select>
                        <div class="invalid-feedback">
                            Por favor, seleccione el estado del usuario.
                        </div>
                    </div>
                    <div class="form-group col-sm-12 col-md-3">
                        <label for="tu">Tipo de usuario:</label>
                        <select id="tu" class="form-control" name="tu" required>
                            <option value="">-- Seleccionar --</option>
                            <option value="1">Administrador</option>
                            <option value="2">Vendedor</option>
                            <option value="3">Reportes</option>
                        </select>
                        <div class="invalid-feedback">
                            Por favor, seleccione el tipo de usuario.
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <button type="submit" class="btn btn-outline-success" name='btnAdd' value="x">
                        <i class="fas fa-plus"></i>
                        Nuevo
                    </button>
                    &nbsp;
                    <button type="button" name="btnEdit" 
                            class="btn btn-outline-info btnAction" value="editar">
                        <i class="fas fa-pencil-alt"></i>
                        Editar
                    </button>
                    &nbsp;
                    <button type="button" name="btnDelete" 
                            class="btn btn-outline-danger btnAction" value="eliminar">
                        <i class="fas fa-trash"></i>
                        Eliminar
                    </button>
                    &nbsp;
                    <button type="reset" class="btn btn-outline-secondary">
                        <i class="fas fa-undo"></i>
                        Cancelar
                    </button>
                </div>
            </form>
            <%
                }
            %>
            <br>
            <!-- Example DataTables Card -->
            <div class="card mb-3">
                <div class="card-header">
                    <i class="fa fa-table"></i> Listado de Clientes
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover" id="cliTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Estado</th>
                                    <th>Tipo Usuario</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Estado</th>
                                    <th>Tipo Usuario</th>
                                    <th>Acciones</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <%
                                    List <Usuario> c = daoC.all(user);
                                    for (Usuario cl : c)
                                    {
                                %>
                                <tr>
                                    <td><%= cl.getNombre() %></td>
                                    <td>
                                        <%
                                            if (cl.getEstado() == 1)
                                            {
                                                out.print("Activado");
                                            }
                                            else if (cl.getEstado() == 2)
                                            {
                                                out.print("Desactivado");
                                            }
                                        %>
                                    </td>
                                    <td><%= cl.getNombreTu()%></td>
                                    <td class="justify-content-center">
                                        <a class="btn btn-outline-primary btn-sm col-12 select" href="javascript:load(<%= cl.getId() %>, '<%= cl.getNombre() %>', '<%= cl.getPass() %>', <%= cl.getEstado()%>, <%= cl.getTu()%>)"> 
                                            Seleccionar
                                        </a>
                                    </td>
                                </tr>
                                <%
                                    }
                                %>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer small text-muted">
                    Última actualización: 
                    <%
                        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
                        String formatDateTime = LocalDateTime.now().format(formatter);
                        out.print(formatDateTime);
                    %>
                </div>
            </div>
        </div>
        <script src="resources/js/jquery-3.3.1.js" type="text/javascript"></script>
        <script src="resources\js\popper.min.js" type="text/javascript"></script>
        <script src="resources\js\bootstrap.min.js" type="text/javascript"></script>
        <script src="resources\js\fontawesome-all.js" type="text/javascript"></script>
        <script src="resources\js\sweetalert2.all.min.js" type="text/javascript"></script>
        <script src="resources/js/datatables.js" type="text/javascript"></script>
        <script src="resources/js/dataTables.bootstrap4.js" type="text/javascript"></script>
        <script src="resources/js/buttons.bootstrap4.js" type="text/javascript"></script>
        <script src="resources/js/usuario.js" type="text/javascript"></script>
        <script src="resources/js/globalConfig.js" type="text/javascript"></script>
        <script>
        <%
            if (request.getSession().getAttribute("info") != null)
            {
                String info = (String) request.getSession().getAttribute("info");
        %>
            swal('Realizado!', '<%= info %>', 'success');
            console.log('Mensaje');
        <%        
            }
            request.getSession().setAttribute("info", null);
            // Error handler
            if (request.getSession().getAttribute("Error") != null)
            {
                String error = (String) request.getSession().getAttribute("Error");
        %>
            swal({
                type: 'error',
                title: 'Error al realizar la operación',
                text: "Ocurrió el siguiente error: \n<%= error %>"
            })
        <%        
            }
            request.getSession().setAttribute("Error", null);
        %>
        </script>
    </body>
</html>
