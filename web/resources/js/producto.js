(function ()
{
    validateForm();
    
    $('.btnCode').click(function ()
    {
        let randomNumber = Math.floor(Math.random() * (999999999999 - 1000000000 + 1)) + 1000000000;
        $('#txtCodigoBarra').val(randomNumber);
    });
    
    $('.btnAction').click(function (e)
    {
        var form = $('#frm')[0];
        sendData(form, $(this), e);
    });
    
    $('.select').click(function (e)
    {
        //window.scrollTo(0, 0);
        $('html, body').animate({scrollTop:0}, 'slow');
    });

    $('#dataTable').DataTable({
        "language":
        {
            "sProcessing": "Procesando...",
            "lengthMenu": "Mostrar _MENU_ registros por pagina",
            "zeroRecords": "No se encontraron resultados",
            "info": "Mostrando pagina _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrando de un total de _MAX_ registros)",
            "oPaginate":
            {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "sSearch": "Buscar:",
            "oAria":
            {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });
})(jQuery);

function cargar(id, nombre, precio, stockMin, stockActu, codigoBar)
{
    $('#txtId').val(id);
    $('#txtNombre').val(nombre);
    $('#txtPrecio').val(precio);
    $('#txtStockMinimo').val(stockMin);
    $('#txtStockActual').val(stockActu);
    $('#txtCodigoBarra').val(codigoBar);
}

// Funcion para enviar datos
function sendData(form, btn, e)
{
    if (form.checkValidity() === false)
    {
        e.preventDefault();
        e.stopPropagation();
    }
    else
    {
        swal({
            title: 'Seguro que desea ' + btn.val() + ' el registro?',
            text: "Si selecciona 'Aceptar', la transaccion será realizada.",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#138496',
            cancelButtonColor: '#5A6268',
            confirmButtonText: 'Aceptar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.value)
            {
                $('#action').val($(btn).val());
                $('#frm').submit();
            }
        });
    }
    form.classList.add('was-validated');
}

function validateForm()
{
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = $('.needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form)
    {
        form.addEventListener('submit', function(event)
        {
          if (form.checkValidity() === false)
          {
            event.preventDefault();
            event.stopPropagation();
          }
          form.classList.add('was-validated');
        }, false);
    });
}