(function()
{
    validateForm();
    
    reset();
    
    $('.seeDetails').click(function ()
    {
        let num = $(this).attr("data-num");
        location.href = 'detalle.jsp?n=' + num;
    });
    
    $('.btnCancel').click(function ()
    {
        reset();
    });
    
    $('.btnCode').click(function ()
    {
        let randomNumber = Math.floor(Math.random() * (999999999999 - 1000000000 + 1)) + 1000000000;
        $('#num').val(randomNumber);
    });
    
    // Inicializando tabla
    $("#dataTable").DataTable(
    {
        "language":
        {
            "sProcessing": "Procesando...",
            "lengthMenu": "Mostrar _MENU_ registros por pagina",
            "zeroRecords": "No se encontraron resultados",
            "info": "Mostrando pagina _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrando de un total de _MAX_ registros)",
            "oPaginate":
            {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "sSearch": "Buscar:",
            "oAria":
            {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });
})(jQuery);

// Funcion para cargar datos
function load(id, num, cId, vId, tot, fech)
{
    $('#fId').val(id);
    $('#num').val(num);
    $('#cId').val(cId);
    $('#vId').val(vId);
    $('#total').val(tot);
    $('#fecha').val(moment(fech, 'YYYY-MM-DD'));
}

function reset()
{
    $('#fId').val("0");
    $('#num').val("");
    $('#cId').val("");
    $('#vId').val("");
    $('#total').val("0.0");
    $('#fecha').val(moment().format('YYYY-MM-DD'));
}

function validateForm()
{
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = $('.needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form)
    {
        form.addEventListener('submit', function(event)
        {
          if (form.checkValidity() === false)
          {
            event.preventDefault();
            event.stopPropagation();
          }
          form.classList.add('was-validated');
        }, false);
    });
}
