<%-- 
    Document   : vendedor
    Created on : 09-26-2018, 07:44:28 PM
    Author     : Perez
--%>

<%@page import="java.time.LocalDateTime"%>
<%@page import="java.time.format.DateTimeFormatter"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.models.*;" %>
<%@page session="true" %>

<% 
    HttpSession ss = request.getSession();
    Integer idTu = -1;
    String user = "";
    if (ss.getAttribute("tu") != null || ss.getAttribute("user") != null)
    {
        idTu = (Integer) ss.getAttribute("tu");
        user = (String) ss.getAttribute("user");
    }
    else
    {
        response.sendRedirect("login.jsp");
    }
    DaoVendedor daoV = new DaoVendedor();
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" type="image/png" href="resources/img/Icon-shop.png" sizes="64x64">
        <link href="resources\css\bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="resources/css/sweetalert2.min.css" rel="stylesheet" type="text/css">
        <link href="resources/css/datatables.css" rel="stylesheet" type="text/css"/>
        <link href="resources/css/dataTables.bootstrap4.css"/>
        <link href="resources/css/buttons.bootstrap4.css" rel="stylesheet" type="text/css"/>
        <title>Vendedor</title>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="#">
                <i class="fas fa-store"></i>
                Tienda
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="index.jsp">
                            <i class="fas fa-home    "></i>
                            Inicio <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="producto.jsp">
                            <i class="fas fa-shopping-basket"></i>
                            Productos
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="vendedor.jsp">
                            <i class="fas fa-id-card-alt"></i>
                            Vendedores
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="cliente.jsp">
                            <i class="fas fa-users"></i>
                            Clientes
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="venta.jsp">
                            <i class="fas fa-receipt"></i>
                            Venta
                        </a>
                    </li>
                    <%
                        if (idTu == 1)
                        {
                    %>
                    <li class="nav-item">
                        <a class="nav-link" href="usuario.jsp">
                            <i class="fas fa-user"></i>
                            Usuarios
                        </a>
                    </li>
                    <%
                        }
                    %>
                </ul>
                <form action="controllerUsuario" id="closeFrm" method="post" class="form-inline my-2 my-lg-0">
                    <input type="text" name="close" value="close" style="display: none;">
                    <button class="btn btn-outline-light btn-sm my-2 my-sm-0" 
                            name="close" value="close" type="button" id="closeSession">
                        <i class="fas fa-sign-out-alt"></i>
                        Cerrar sesion
                    </button>
                </form>
            </div>
        </nav>
        <div class="container">
            <br>
            <br>
            <center>
                <h1 class="text-dark">
                    <i class="fas fa-id-card-alt"></i>
                    Vendedores
                </h1>
            </center>
            <br>
            <%
                if (idTu == 1)
                {
            %>
            <form action="controllerVendedor" class="needs-validation" id="frm" name="frm" method="post" novalidate>
                <div class="form-row">
                    <div class="form-group col-md-4" style="display: none;">
                        <input type="text" class="form-control" id="txtId" name="txtId" value="0" readonly required/>
                        <input type="text" name="action" id="action"/>
                    </div>
                    <div class="form-group col-md-4">
                        <label>DUI:</label>
                        <input type="text" data-mask="00000000-0" placeholder="00000000-0" 
                               class="form-control" id="txtDui" name="txtDui" required/>
                        <div class="invalid-feedback">
                            Por favor, ingrese el DUI del vendedor.
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label>Nombre:</label>
                        <input type="text" placeholder="Nombre" class="form-control" 
                               id="txtNombre" name="txtNombre" required>
                        <div class="invalid-feedback">
                            Por favor, ingrese el nombre del vendedor.
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label>Apellidos:</label>
                        <input type="text" placeholder="Apellidos" class="form-control" 
                               id="txtApellidos" name="txtApellidos" required>
                        <div class="invalid-feedback">
                            Por favor, ingrese los apellidos del vendedor.
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label>Direccion:</label>
                        <input type="text" placeholder="Direccion" class="form-control" 
                               id="txtDireccion" name="txtDireccion" required>
                        <div class="invalid-feedback">
                            Por favor, ingrese la dirección del vendedor.
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label>Telefono Oficina:</label>
                        <input type="text" placeholder="0000-0000" data-mask="0000-0000" 
                               class="form-control" id="txtTelOficina" name="txtTelOficina" required/>
                        <div class="invalid-feedback">
                            Por favor, ingrese el teléfono de oficina del vendedor.
                        </div>
                    </div>
                     <div class="form-group col-md-4">
                        <label>Telefono Movil:</label>
                        <input type="text" placeholder="0000-0000" data-mask="0000-0000" 
                               class="form-control" id="txtTelMovil" name="txtTelMovil" required/>
                        <div class="invalid-feedback">
                            Por favor, ingrese el teléfono movil del vendedor.
                        </div>
                    </div>
                </div>
                <center>
                    <button type="submit" name="btnAdd" value="Agregar" class="btn btn-outline-success">
                        <i class="fas fa-plus"></i>
                        Nuevo
                    </button>
                    <button type="button" name="btnEdit" value="editar" class="btn btn-outline-info btnAction">
                        <i class="fas fa-pencil-alt"></i>
                        Editar
                    </button>
                    <button type="button" name="btnDelete" value="eliminar" 
                            class="btn btn-outline-danger btnAction">
                        <i class="fas fa-trash"></i>
                        Eliminar
                    </button>
                    <button type="reset" class="btn btn-outline-secondary">
                        <i class="fas fa-undo"></i>
                        Cancelar
                    </button>
                </center>
            </form>
            <br>
            <%
                }
            %>
            <!-- Example DataTables Card -->
            <div class="card mb-3">
                <div class="card-header">
                    <i class="fa fa-table"></i> Listado de Clientes
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                            <table id="dataTable" class="table table-hover text-center">
                                <thead class="">
                                    <th scope="col">ID</th>
                                    <th scope="col">DUI</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Apellidos</th>
                                    <th scope="col">Direccion</th>
                                    <th scope="col">Telefono Oficina</th>
                                    <th scope="col">Telefono Movil</th>
                                    <th scope="col">Seleccionar</th>
                                </thead>
                                <tfoot class="">
                                    <th scope="col">ID</th>
                                    <th scope="col">DUI</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Apellidos</th>
                                    <th scope="col">Direccion</th>
                                    <th scope="col">Telefono Oficina</th>
                                    <th scope="col">Telefono Movil</th>
                                    <th scope="col">Seleccionar</th>
                                </tfoot>
                                <tbody>
                                <%
                                    List<Vendedor> lst = daoV.all();
                                    for(Vendedor vende : lst)
                                    {
                                %>
                                <tr>
                                    <td><%= vende.getId()%></td>
                                    <td><%= vende.getDui()%></td>
                                    <td><%= vende.getNombre()%></td>
                                    <td><%= vende.getApellidos()%></td>
                                    <td><%= vende.getDireccion()%></td>
                                    <td><%= vende.getTelOficina()%></td>
                                    <td><%= vende.getTelMovil()%></td>
                                    <td><a class="btn btn-outline-primary btn-sm col-12 select" href="javascript:load(<%=vende.getId()%>,'<%=vende.getDui()%>','<%=vende.getNombre()%>',
                                           '<%=vende.getApellidos()%>','<%=vende.getDireccion()%>','<%= vende.getTelOficina()%>','<%= vende.getTelMovil()%>')">Seleccionar</a></td>
                                </tr>
                                <%
                                    }
                                %>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer small text-muted">
                        Última actualización: 
                        <%
                            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
                            String formatDateTime = LocalDateTime.now().format(formatter);
                            out.print(formatDateTime);
                        %>
                    </div>
                </div>   
            </div>
        </div>
        <script src="resources/js/jquery-3.3.1.js" type="text/javascript"></script>
        <script src="resources/js/jquery.mask.js" type="text/javascript"></script>
        <script src="resources/js/popper.min.js" type="text/javascript"></script>
        <script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="resources/js/fontawesome-all.js" type="text/javascript"></script>
        <script src="resources/js/sweetalert2.all.min.js" type="text/javascript"></script>
        <script src="resources/js/datatables.js" type="text/javascript"></script>
        <script src="resources/js/dataTables.bootstrap4.js" type="text/javascript"></script>
        <script src="resources/js/buttons.bootstrap4.js" type="text/javascript"></script>
        <script src="resources/js/vendedor.js" type="text/javascript"></script>
        <script src="resources/js/globalConfig.js" type="text/javascript"></script>
        <script>
        <%
        if (request.getSession().getAttribute("info") != null)
        {
            String info = (String) request.getSession().getAttribute("info");
        %>
            swal('Realizado!', '<%= info %>', 'success');
        <%        
            }
            request.getSession().setAttribute("info", null);
            // Error handler
            if (request.getSession().getAttribute("Error") != null)
            {
                String error = (String) request.getSession().getAttribute("Error");
        %>
            swal({
                type: 'error',
                title: 'Error al realizar la operación',
                text: "Ocurrió el siguiente error: \n" + "<%= error %>",
            })
        <%        
            }
            request.getSession().setAttribute("Error", null);
        %>
        </script>
    </body>
</html>
