CREATE DATABASE TareaEvaluada2;
USE TareaEvaluada2;

-- == == == == == == == == == == == == == == == == == == == ==

--
-- Estructura de tabla para la tabla `Cliente`
--

CREATE TABLE IF NOT EXISTS Cliente
(
	id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(50) NOT NULL,
    apellidos VARCHAR(50) NOT NULL,
    direccion VARCHAR(50) NOT NULL,
    PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO Cliente VALUES(NULL, 'Juan', 'Elias', 'La Libertad');
INSERT INTO Cliente VALUES(NULL, 'Kevin', 'Lovo', 'San Salvador');
INSERT INTO Cliente VALUES(NULL, 'Alvaro', 'Perez', 'Soyapango');

-- == == == == == == == == == == == == == == == == == == == ==

--
-- Estructura de tabla para la tabla `Vendedor`
--

CREATE TABLE IF NOT EXISTS Vendedor
(
	id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    dui VARCHAR(10) UNIQUE NOT NULL,
    nombre VARCHAR(50) NOT NULL,
    apellidos VARCHAR(50) NOT NULL,
    direccion VARCHAR(50) NOT NULL,
    telOficina VARCHAR(50) NOT NULL,
    telMovil VARCHAR(50) NOT NULL,
    PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO Vendedor VALUES(NULL, '12345678-9', 'Rachel', 'Martinez', 'Santa Ana', '1212-3434', '5656-7878');
INSERT INTO Vendedor VALUES(NULL, '98765432-1', 'Flor', 'Flores', 'Santa Tecla', '1111-1111', '2222-2222');
INSERT INTO Vendedor VALUES(NULL, '12121212-1', 'Alexis', 'Fernandez', 'Sonsonate', '3333-3333', '4444-4444');

-- == == == == == == == == == == == == == == == == == == == ==

--
-- Estructura de tabla para la tabla `Producto`
--

CREATE TABLE IF NOT EXISTS Producto
(
	id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(50) NOT NULL,
    precioVenta DOUBLE NOT NULL,
    stockMin INT NOT NULL,
    stockAct INT NOT NULL,
    codigoBarra VARCHAR(50) UNIQUE NOT NULL,
    PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO Producto VALUES(NULL, 'Leche Nido', 2.0, 100, 500, 'u1v5q9lk4o98pgma6nh10i');
INSERT INTO Producto VALUES(NULL, 'Carne de res', 5.50, 200, 300, 'lb9d08zgit9zjktbgp0mh');
INSERT INTO Producto VALUES(NULL, 'Galletas', 1.5, 300, 1000, 'bsj74rthnbc2blug5eskg');

-- == == == == == == == == == == == == == == == == == == == ==

--
-- Estructura de tabla para la tabla `Factura`
--

CREATE TABLE IF NOT EXISTS Factura
(
	id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    numFactura VARCHAR(50) UNIQUE NOT NULL,
    idCliente INT UNSIGNED NOT NULL,
    idVendedor INT UNSIGNED NOT NULL,
    total DOUBLE NOT NULL,
    fecha DATE NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (idCliente) REFERENCES Cliente (id)
    ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (idVendedor) REFERENCES Vendedor (id)
    ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO Factura VALUES(NULL, '929641707871', 1, 3, 0.0, '2018-10-2');
INSERT INTO Factura VALUES(NULL, '981061559369', 2, 2, 0.0, '2018-10-2');
INSERT INTO Factura VALUES(NULL, '834609625820', 3, 1, 0.0, '2018-10-2');

-- == == == == == == == == == == == == == == == == == == == ==

--
-- Estructura de tabla para la tabla `DetalleFactura`
--

CREATE TABLE IF NOT EXISTS DetalleFactura
(
	id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    idFactura INT UNSIGNED NOT NULL,
    idProducto INT UNSIGNED NOT NULL,
    cantidad INT NOT NULL,
    subTotal DOUBLE NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (idFactura) REFERENCES Factura (id)
    ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (idProducto) REFERENCES Producto (id)
    ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO DetalleFactura VALUES (NULL, 1, 1, 2, 4.00);
INSERT INTO DetalleFactura VALUES (NULL, 1, 2, 2, 11.00);
INSERT INTO DetalleFactura VALUES (NULL, 1, 3, 2, 4.5);
INSERT INTO DetalleFactura VALUES (NULL, 2, 1, 2, 4.00);
INSERT INTO DetalleFactura VALUES (NULL, 2, 2, 2, 11.00);
INSERT INTO DetalleFactura VALUES (NULL, 2, 3, 2, 4.5);
INSERT INTO DetalleFactura VALUES (NULL, 3, 1, 2, 4.00);
INSERT INTO DetalleFactura VALUES (NULL, 3, 2, 2, 11.00);
INSERT INTO DetalleFactura VALUES (NULL, 3, 3, 2, 4.5);
UPDATE Factura SET total = (SELECT SUM(subTotal) AS Total FROM DetalleFactura WHERE idFactura = 1) WHERE id = 1;
UPDATE Factura SET total = (SELECT SUM(subTotal) AS Total FROM DetalleFactura WHERE idFactura = 2) WHERE id = 2;
UPDATE Factura SET total = (SELECT SUM(subTotal) AS Total FROM DetalleFactura WHERE idFactura = 3) WHERE id = 3;

UPDATE Producto SET stockAct = stockAct - 
	(SELECT SUM(cantidad) FROM DetalleFactura 
    WHERE idProducto = 1) WHERE id = 1;
UPDATE Producto SET stockAct = stockAct - 
	(SELECT SUM(cantidad) FROM DetalleFactura 
    WHERE idProducto = 2) WHERE id = 2;
UPDATE Producto SET stockAct = stockAct - 
	(SELECT SUM(cantidad) FROM DetalleFactura 
    WHERE idProducto = 3) WHERE id = 3;

-- == == == == == == == == == == == == == == == == == == == ==== == == == == == == == == 

--
-- Estructura de tabla para la tabla `TiposUsuario`
--

CREATE TABLE IF NOT EXISTS TiposUsuario
(
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    descripcion VARCHAR(50) NOT NULL,
    PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO TiposUsuario VALUES (NULL, 'Administrador');
INSERT INTO TiposUsuario VALUES (NULL, 'Vendedor');
INSERT INTO TiposUsuario VALUES (NULL, 'Reportes');

-- == == == == == == == == == == == == == == == == == == == ==

--
-- Estructura de tabla para la tabla `Usuarios`
--

CREATE TABLE IF NOT EXISTS Usuarios
(
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    idTipoUsuario INT UNSIGNED NOT NULL,
    nombre VARCHAR(50) UNIQUE NOT NULL,
    pass VARBINARY(250) NOT NULL,
    estado INT NOT NULL,
    PRIMARY KEY (id),
    KEY (idTipoUsuario),
    CONSTRAINT Usuario_TU FOREIGN KEY (idTipoUsuario)
    REFERENCES TiposUsuario(id)
    ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

INSERT INTO Usuarios VALUES (NULL, 1, 'Admin', AES_ENCRYPT('123', 'Key'), 1);
INSERT INTO Usuarios VALUES (NULL, 2, 'Vendedor', AES_ENCRYPT('123', 'Key'), 1);
INSERT INTO Usuarios VALUES (NULL, 3, 'Reportes', AES_ENCRYPT('123', 'Key'), 1);
SELECT U.nombre AS User, CAST(AES_DECRYPT(U.pass, 'Key') AS CHAR(50)) AS Pass FROM Usuarios U WHERE U.pass = AES_ENCRYPT('123', 'Key') AND U.nombre = 'Admin';



-- ===================================================================
SELECT 
	P.nombre AS Nombre, P.precioVenta AS Precio, P.codigoBarra, 
    SUM(DF.cantidad) AS CantVendidos, SUM(DF.subTotal) AS TotalVentas 
FROM 
DetalleFactura DF
INNER JOIN
	Producto P
ON
	DF.idProducto = P.id
GROUP BY
	DF.idProducto
ORDER BY
	CantVendidos
DESC;

-- ===================================================================
SELECT 
	V.nombre AS Nombre, V.apellidos AS Apellidos,
    V.dui AS DUI, V.telMovil AS Telefono, 
    COUNT(F.idVendedor) AS CantVentas
FROM 
	Factura F
INNER JOIN
	Vendedor V
ON
	F.idVendedor = V.id
GROUP BY
	F.idVendedor
ORDER BY
	CantVentas
DESC LIMIT 5;

-- ===================================================================
SELECT 
	F.numFactura, F.total, F.fecha, 
    C.nombre AS NombreCliente, V.nombre AS NombreVendedor
FROM 
	Factura F
INNER JOIN
	Cliente C              
ON
	F.idCliente = C.id
INNER JOIN
	Vendedor V             
ON
	F.idVendedor = V.id
WHERE
	F.idCliente = 2
ORDER BY
	F.idCliente
DESC;


-- ===================================================================
SELECT 
	F.numFactura, F.total, DATE_FORMAT(F.fecha, '%d/%m/%Y') AS Fecha, 
    C.nombre AS NombreCliente, V.nombre AS NombreVendedor
FROM 
	Factura F
INNER JOIN
	Cliente C              
ON
	F.idCliente = C.id
INNER JOIN
	Vendedor V             
ON
	F.idVendedor = V.id
WHERE
	F.fecha BETWEEN '2018-10-2' AND '2018-10-6'
ORDER BY
	F.fecha
DESC;

-- ===================================================================
SELECT U.id, U.idTipoUsuario AS tu, 
U.nombre AS User, CAST(AES_DECRYPT(U.pass, 'Key') 
AS CHAR(50)) AS Pass, U.estado, TU.descripcion AS NombreTu
FROM Usuarios U INNER JOIN TiposUsuario TU
ON U.idTipoUsuario = TU.id ORDER BY U.id DESC;

